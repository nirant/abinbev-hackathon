import os
from PIL import Image

import numpy as np
import matplotlib.pyplot as plt
from skimage import io
from scipy import misc
from skimage.filters import threshold_otsu

logFlag = True

def readAllJpgLocation(location):
    ret = []
    for file in os.listdir(location):
        if file.endswith(".jpg"):
            ret.append(file)
    return ret

def average(pixel):
    return (pixel[0] + pixel[1] + pixel[2]) / 3

def weightedAverage(pixel):
    return 0.299*pixel[0] + 0.587*pixel[1] + 0.114*pixel[2]

def processStrip(imageLocation, threashold=100):
    result = []
    image = misc.imread(imageLocation)
    imToCrop = Image.open(imageLocation)

    grey = np.zeros((image.shape[0], image.shape[1])) # init 2D numpy array
    for rownum in range(len(image)):
        for colnum in range(len(image[rownum])):
            grey[rownum][colnum] = weightedAverage(image[rownum][colnum])

    thresh = threshold_otsu(grey)
    binary = grey > thresh

    binary_code = 1*binary

    B = np.asmatrix(binary_code)
    a = np.sum(binary_code, axis=0)

    a[a < a.mean()] = 0
    a[a > a.mean()] = 1

    prev_i = a[0]
    next_i = a[0]
    left = []
    right = []

    for i in range(1, len(a)):
        prev_i = a[i-1]
        next_i = a[i]
        if prev_i < next_i:
            left.append(i)
        if prev_i > next_i:
            right.append(i)
            i = i+1

    if len(left) != len(right):
        right.append(len(a))

    width, height = imToCrop.size
    for i in range(0, len(left)):
        if (right[i] - left[i]) > threashold:
            temp = imToCrop.crop((left[i], 0, right[i], height))
            result.append(temp)
    return result


def getStrips(im):
    width, height = im.size
    cropim1 = im.crop((0, 717, width, 816))
    # cropim1.save("../result/1"+ image)
    cropim2 = im.crop((0, 1769, width, 1895))
    # cropim2.save("../result/2"+ image)
    cropim3 = im.crop((0, 2785, width, 2911))
    # cropim3.save("../result/3"+ image)
    return [cropim1, cropim2, cropim3]

def preprocessImages(inFolder, outFolder):
    #get all images from the folder
    preResult = []
    images = readAllJpgLocation(inFolder)
    for image in images:
        count = 0
        im = Image.open(inFolder+image)
        strips = getStrips(im)
        if logFlag:
            print "processing " + image + " : " + str(len(strips))
        for item in range(0, len(strips)):
            # save strip to temp file
            strips[item].save("../temp.jpg")
            # processStrip
            res = processStrip("../temp.jpg")
            for j in range(0, len(res)):
                res[j].save(outFolder+str(count)+"_"+image)
                count = count+1
            # delete the temp file
            os.remove("../temp.jpg")

if __name__ == '__main__':
    preprocessImages("../img dataset/", "../tags/")
