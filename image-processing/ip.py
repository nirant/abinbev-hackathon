#http://scikit-learn.org/stable/auto_examples/model_selection/plot_learning_curve.html

#http://machinelearningmastery.com/feature-selection-machine-learning-python/

from skimage import io
from scipy import misc
import matplotlib.pyplot as plt
import numpy as np

image = misc.imread('/Users/hackerrank/Downloads/test.png')

def average(pixel):
  return (pixel[0] + pixel[1] + pixel[2]) / 3

def weightedAverage(pixel):
  return 0.299*pixel[0] + 0.587*pixel[1] + 0.114*pixel[2]

grey = np.zeros((image.shape[0], image.shape[1])) # init 2D numpy array

for rownum in range(len(image)):
   for colnum in range(len(image[rownum])):
      grey[rownum][colnum] = weightedAverage(image[rownum][colnum])

from skimage.filters import threshold_otsu

thresh = threshold_otsu(grey)
binary = grey > thresh

binary_code = 1*binary

B = np.asmatrix(binary_code)
a = np.sum(binary_code, axis=0)

a[a < a.mean()] = 0
a[a > a.mean()] = 1

prev_i = a[0]
next_i = a[0]
left = []
right = []

for i in range(1,len(a)):
  prev_i = a[i-1]
  next_i = a[i]
  if (prev_i < next_i):
    left.append(i)
  if (prev_i > next_i):
    right.append(i)
    i = i+1

if (len(left) != len(right)):
  right.append(len(a))

left
right
