import pytesseract
import datetime
import time
import os
from scipy.misc import imsave
import re
from subprocess import call
import numpy as np
import ntpath
import cv2
import re

logFlag = True

error = []

orderDaru = [["SIERRA NEVADA PALE ALE", "Glass"],
			["SIERRA NEVADA TORPEDO", "Glass"],
			["BLUE POINT TOASTED", "Glass"],
			["MAGIC HAT", "Glass"],
			["BROOKLYN PILSNER", "Glass"],
			["BROOKLYN INDIA PALE", "Glass"],
			["BROOKLYN OKTOBERFEST", "Glass"],
			["CORONA EXTRA", "Glass"],
			["CORONA EXTRA", "Glass"],
			["CORONA LIGHT", "Glass"],
			["STELLA ARTOIS", "Glass"],
			["STELLA ARTOIS", "Glass"],
			["LEFFE BLONDE", "Glass"],
			["HOEGAARDEN", "Glass"],
			["BECKS", "Glass"],
			["MILLER LITE", "Glass"],
			["COORS LIGHT", "Glass"],
			["BUD LIGHT PLATINUM", "Glass"],
			["BUD LIGHT", "Glass"],
			["BUDWEISER", "Glass"],
			["MILLER LITE", "Glass"],
			["MILLER LITE", "Can"]]

class Daaru:
	def __init__(self, name, packs, volume, price, cg, week):
		self.name = str(name)
		self.packs = str(packs)
		self.volume = str(volume)
		self.price = str(price)
		self.cg = str(cg)
		self.week = str(week)
	
	def __str__(self):
		ret = "Name: " + self.name + "\n"
		ret = ret + "Week: " + self.week + "\n"
 		ret = ret + "Pack: " + self.packs + "\n"
		ret = ret + "Volume: " + self.volume + "\n"
		ret = ret + "Price: " + self.price + "\n"
		ret = ret + "Type of Packaging: " + self.cg + "\n"
		return ret
	
	def getCSV(self):
		ret = self.week + "," + self.name + ","
		ret = ret + self.packs + " Pk " + self.volume + " Oz " + self.cg + ","
		ret = ret + self.price + '\n'
		return ret

def gamma_correction(img, correction):
    img = img/255.0
    img = cv2.pow(img, correction)
    return np.uint8(img*255)

def cleanse_string(str):
	useless_characters = "~!@#$%^&*(){.}`:<.>?,./;'[]|\\"
	char_set = set()
	for c in useless_characters:
		char_set.add(c)

	ret = ""
	for i in range(0,len(str)):
		if str[i] not in char_set:
			ret =  ret + str[i]
	return ret

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
 
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
 
	return False


def cleanse_price(str):
	f = str.find("$")
	if f == -1:
		return ("", "")
	str = str[f+1:]
	f = str.find(" ")
	cents= ""
	dollar = ""
	if f == -1:
		cents = str[-2:]
		dollar = str[:2]

	else :
		dollar = str[0:f]
		cents = str[f+1:]
	return (dollar, cents)

def get_daaru_name(str1, str2):
	if len(str1) < 2:
		str1 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
	if len(str2) < 2:
		str2 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

	daaru_names = ["SIERRA NEVADA TORPEDO",
	"BROOKLYN OKTOBERFEST",
	"CORONA EXTRA",
	"LEFFE BLONDE",
	"BROOKLYN PILSNER",
	"CORONA LIGHT",
	"BECKS",
	"SIERRA NEVADA PALE ALE",
	"BUD LIGHT",
	"BUD LIGHT PLATINUM",
	"HOEGAARDEN",
	"MAGIC HAT",
	"BLUE POINT TOASTED",
	"STELLA ARTOIS",
	"BUDWEISER",
	"COORS LIGHT",
	"MILLER LITE",
	"BROOKLYN INDIA PALE"]
	min_dist = 1000000
	ans = ""
	for a in daaru_names:
		l_dist = min(edit_distance(a, str1), edit_distance(a,str2))
		#print str(l_dist) +  " <- " + a + " :: " + str1 +" /|\ " +str2 
		if l_dist < min_dist:
			min_dist = l_dist
			ans = a
	return ans

def edit_distance(s1, s2):
	s1 = cleanse_string(s1.lower().strip())
	s2 = cleanse_string(s2.lower().strip())
	m=len(s1)+1
	n=len(s2)+1
	tbl = {}
	for i in range(m): tbl[i,0]=i
	for j in range(n): tbl[0,j]=j
	for i in range(1, m):
		for j in range(1, n):
			cost = 0 if s1[i-1] == s2[j-1] else 1
			tbl[i,j] = min(tbl[i, j-1]+1, tbl[i-1, j]+1, tbl[i-1, j-1]+cost)
	return tbl[i,j]

def get_list(output_file):
	cnt = 0
	l = []
	with open(output_file) as f:
		for line in f:
			if(len(line.strip()) < 3 ):
				continue
			else:
			 	if cnt == 0 :
			 		cnt = cnt + 1
			 		l.append(line.replace("\n", ""))
			 	elif cnt == 1 :
			 		cnt = cnt + 1
			 		l.append(line.replace("\n", ""))
			 	elif cnt == 2 :
			 		cnt = cnt + 1
			 		l.append(line.replace("\n", ""))
	return l

def get_price(price1, price2):
	price1 = price1.lower().strip()
	price2 = price2.lower().strip()
	dollar1, cents1  = cleanse_price(price1)
	dollar2, cents2 = cleanse_price(price2)
	dollar = dollar1 if is_number(dollar1) else dollar2
	cents = ""
	if cents1[:1] != "9" and cents2[:1] == "9" :
		cents = cents2
	elif cents2[:1] != "9" and cents1[:1] == "9":
		cents = cents1
 	else:
 		cents = cents1 if is_number(cents1) else cents2
 	if cents[-1:] != "9" and len(cents) > 1:
 		cents = cents[0:len(cents)-1] + "9"
	if is_number(cents) == False:
		cents  = "99"
 	return dollar + "." + cents

def process_packs_volume(str):
	str = str.lower().strip()
	f = str.find("pk")
	if f == -1:
		f = str.find("pm")

	left = str[0:f].strip()
	l_list = re.findall(r'\d+', left)
	if len(l_list) > 0:
		left = l_list[0]
	else :
		left = "6"
	right = str[f+3:].strip()
	cg = ""
	if right.find("ass") == -1:
		cg = "Can"
	elif right.find("an") == -1:
		cg = "Glass"
	data = right.split()
	if len(data) > 1 and is_number(data[1]) and data[1][0] != '0':
		return left, data[0]+"."+data[1], cg
	elif len(data) > 0:
		return left, data[0], cg
	else:
		return left, "12", cg



def get_packs_volume(str1, str2):
	packs1,vol1,cg1 = process_packs_volume(str1)
	packs2,vol2,cg2 = process_packs_volume(str2)
	if packs1 == "6" or packs2 == "6":
		packs = "6"
	else :
		packs = packs1 if is_number(packs1)  else packs2
	vol = vol1 if is_number(vol1)  else vol2
	cg = ""
	if len(cg1) == 0 and len(cg2) != 0 :
		cg = cg2
	if len(cg2) == 0 and len(cg1) != 0 :
		cg = cg1
	if len(cg1) !=0 and len(cg2) != 0 and cg1 != cg2:
		cg = "Glass"
	elif cg1 == cg2:
		cg = cg1
	if len(cg) == 0:
		cg = "Glass"
	return (packs, vol, cg)

def getNumWeek(filename):
    	week = get_week(filename)
	var = ntpath.basename(filename)
	num = int(re.search(r'\d+', var).group())
	return num, week

def get_week(imageName):
	var = ntpath.basename(imageName)
	var = var.lstrip('0123456789._- ')
	var = var[12:-4]
	month = var[5:-3]
	if int(month) < 10:
		month = month[1]
	day = var[-2:]
	if int(day) < 10:
		day = day[1]
	year = var[:4]
	# return var[5:-3] + "/" + var[-2:] + "/" + var[:4]
	return month+"/"+day+"/"+year

def identifyErrorWeek(key, name, cg):
    #extract week from imName
	# print key
	# print orderDaru[key[0]][0] + "::" +  orderDaru[key[0]][1]  + "||"+name+"::"+cg 
	if name.lower().strip() != orderDaru[key[0]][0].lower().strip() or cg.lower().strip() != orderDaru[key[0]][1].lower().strip():
    		error.append({
				"week": key[1],
				"id": key[0]
			})


def get_text(image, key, baseOutLocation="../tags_text/"):
	# if image[0] == ".":
	# 	return

	img = cv2.imread(image, 0)
	dst = gamma_correction(img, 0.7)

	ret_gamma,thr_gamma =cv2.threshold(dst,0,255,cv2.THRESH_OTSU)
	ret,thr =cv2.threshold(img,0,255,cv2.THRESH_OTSU)
	imsave('temp_gamma.jpg', thr_gamma)
	imsave('temp.jpg', thr)  # convert image to monochrome
	call(['tesseract', 'temp.jpg', 'out'])
	call(['tesseract', 'temp_gamma.jpg', 'out_gamma'])
	# call(['tesseract', 'temp.jpg', baseOutLocation + "gamma/" + ntpath.basename(image) + 'out'])
	# call(['tesseract', 'temp_gamma.jpg', baseOutLocation + "normal/" + ntpath.basename(image) +'out_gamma'])
	cnt = 0
	name = ""
	packs = ""
	volume = ""
	price = ""
	gamma_list = []
	non_gamma_list = []
	gamma_list = get_list('out_gamma.txt')
	non_gamma_list = get_list('out.txt')
	#print(non_gamma_list)
	#print(gamma_list)
	# Size of array check
	packs = "6"
	volume = "12"
	cg = "Glass"
	if len(gamma_list) < 1 or len(non_gamma_list) < 1 :
		return None
	name = get_daaru_name(gamma_list[0], non_gamma_list[0])
	if len(gamma_list) >= 2 and len(non_gamma_list) >= 2:
		packs, volume, cg = get_packs_volume(gamma_list[1], non_gamma_list[1])
	if is_number(packs) == False:
		packs = "6"
	if packs == "5":
		packs = "6"
	if is_number(volume) == False:
		volume = "12"
	if len(gamma_list) < 3:
		gamma_list.append("")
	if len(non_gamma_list) < 3:
		non_gamma_list.append("")
	if len(gamma_list) >= 3 and len(non_gamma_list) >= 3:
		price = get_price(gamma_list[2], non_gamma_list[2])
	else:
    		price = -1
	if volume == "2":
		volume = "12"
	if packs == "2" or packs == "1":
		packs = "12"
	
	# check for discrepency
	identifyErrorWeek(key, name, cg)
	if logFlag:
		# print "------------"
		# print name
		# print packs + " " + volume + " " + cg
		# print price
		# print "------------"
		print image
	return Daaru(name, packs, volume, price, cg, get_week(image))

def processTags(inFolder, outFile="../output.csv", errorFile="../errorOutput.csv"):
	out = open(outFile, "w")
	fileList = {}
	for file in os.listdir(inFolder):
		if file.endswith(".jpg"):
			fileList[getNumWeek(file)] = file
	for key in fileList:
		var = get_text(inFolder+fileList[key], key)
		if var == None:
    			out.write("\n")
		else:
			out.write(var.getCSV())	
	out.close()
	errorContent = ""
	for item in error:
    		errorContent = errorContent + item["week"] + "," + str(item["id"]) + "\n"
	errorOut = open(errorFile, "w")
	errorOut.write(errorContent)
	errorOut.close()


if __name__ == "__main__":
	processTags("../tags/")
