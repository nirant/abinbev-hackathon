Pitching Workshop Notes
---
1. be passionate, be proud, portray that to the judges, smile, be energetic
2. know who is pitching, make a slide deck, practice the transitions
3. think about the pitch early, start writing pitch ideas
4. rest up
- specially for the pitcher
5. golden points: 
- 30 seconds on product/solution
- 1 minutes on demo
- 30 seconds on next steps, what next? Who is going to use it? Who can make money on this? 
6. importance of next steps
- most frequently asked question
- what is the thought process behind this?
- ** why are you different? 
7. Don't memorize, use guideposts. Practice. Practice. 
- practice with the mentor
8. do two things at once
- don't explain that's a dashboard, what's next
- don't explain landing pages
- don't need to introduce everyone's names
9. anticipate questions
- how are you gonna make money on this? why you?
- research other hackathons
10. practice with yourself and your team
- think in different directions
- can ask other teams if something is exciting for this

Q&A
Stick to front end in the demo. Focus on the solution. 
What focus did you attack? End to end solution